
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    rehna: cc.Node = null;

    @property({type: cc.Node})
    closeButton: cc.Node = null;

    @property({type: cc.Node})
    tryAgainButton: cc.Node = null;

    @property({type: cc.Node})
    nextButton: cc.Node = null;
    
    @property(cc.Node)
    qtext_node: cc.Node = null;

    @property(cc.Label)
    qtext: cc.Label = null;

    @property(cc.Node)
    ftext_node: cc.Node = null;

    @property(cc.Label)
    Nope: cc.Label = null;
    
    @property(cc.Label)
    Correct: cc.Label = null;

    @property(cc.Label)
    ftext: cc.Label = null;

    @property({type: cc.AudioClip})
    BGM: cc.AudioClip = null;

    @property({type: cc.Node})
    DryBG: cc.Node = null;

    @property({type: cc.Node})
    SnowBG: cc.Node = null;

    @property({type: cc.AudioClip})
    introDialogue1: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    introDialogue2: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1Dialogue1: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1Dialogue2: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q2Dialogue1: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q2Dialogue2: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1igloo: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1igloo_melting: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1woodhouse: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q1mudhouse: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q2igloo: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q2tree: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    Q2boat: cc.AudioClip = null;

    @property({type: cc.Node})
    Q1option1: cc.Node = null;

    @property({type: cc.Node})
    Q1option2: cc.Node = null;

    @property({type: cc.Node})
    Q1option3: cc.Node = null;

    @property({type: cc.Node})
    Q2option1: cc.Node = null;

    @property({type: cc.Node})
    Q2option2: cc.Node = null;

    @property({type: cc.Node})
    Q2option3: cc.Node = null;

    @property({type: cc.Node})
    option1box: cc.Node = null;

    @property({type: cc.Node})
    option2box: cc.Node = null;

    @property({type: cc.Node})
    option3box: cc.Node = null;

    @property({type: cc.AudioClip})
    wrongsfx: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    correctsfx: cc.AudioClip = null;

    @property({type: cc.Node})
    igloo: cc.Node = null;

    @property({type: cc.Node})
    igloo_cut: cc.Node = null;

    @property({type: cc.Node})
    woodHouse: cc.Node = null;

    @property({type: cc.Node})
    treeHouse: cc.Node = null;

    @property({type: cc.Node})
    boatHouse: cc.Node = null;

    @property({type: cc.Node})
    mudHouse: cc.Node = null;

    @property({type: cc.Node})
    mudHouse_cut: cc.Node = null;

    @property({type: cc.Node})
    smoke1: cc.Node = null;

    @property({type: cc.Node})
    smoke2: cc.Node = null;

    @property({type: cc.Node})
    smoke3: cc.Node = null;

    @property({type: cc.Node})
    wind: cc.Node = null;

    rehna_anim: cc.Animation = null;
    igloo_anim: cc.Animation = null;
    treeHouse_anim: cc.Animation = null;
    boatHouse_anim: cc.Animation = null;
    smoke1_anim: cc.Animation = null;
    smoke2_anim: cc.Animation = null;
    smoke3_anim: cc.Animation = null;
    wind_anim: cc.Animation = null;
    Q1completed: boolean = false;
    question_no: number = null;

    onLoad () {
        this.rehna_anim = this.rehna.getComponent(cc.Animation);
        this.igloo_anim = this.igloo.getComponent(cc.Animation);
        this.treeHouse_anim = this.treeHouse.getComponent(cc.Animation);
        this.boatHouse_anim = this.boatHouse.getComponent(cc.Animation);
        this.smoke1_anim = this.smoke1.getComponent(cc.Animation);
        this.smoke2_anim = this.smoke2.getComponent(cc.Animation);
        this.smoke3_anim = this.smoke3.getComponent(cc.Animation);
        this.wind_anim = this.wind.getComponent(cc.Animation);
        this.closeButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
            cc.director.loadScene("game")
        });
        cc.macro.ENABLE_MULTI_TOUCH = false;
        this.question_no = 1;
    }

    intro_walking () {
        //character walking to the right
        cc.tween(this.rehna).call(() => {
            this.rehna_anim.play("intro-walk");
        }).to(3, { position: cc.v2(200, -150) })
          .call(() => {
            this.rehna_anim.play("intro-idle");
        }).start();
    }

    intro_text (delay: number) {
        this.qtext_node.setPosition(cc.v2(-1500,350));
        cc.tween(this.qtext_node)
        .to(0.5, {
            position: cc.v2(0,350),
            opacity: 255
        })
        .delay(delay)
        .to(0.5, {
            position: cc.v2(1500,350),
            opacity: 0 }).start();
    }

    feedback_text () {
        this.ftext_node.setPosition(cc.v2(0,-500));
        cc.tween(this.ftext_node)
        .to(0.5, {
            position: cc.v2(0,-150),
            opacity: 255
        }).start();
    }

    // tween the three options of questions upwards
    optionTween (question: number) {
        if(question == 1) {
        cc.tween(this.Q1option1).to(1, {position: cc.v2(-200,-250)}).start();
        cc.tween(this.Q1option2).to(1, {position: cc.v2(0,-250)}).start();
        cc.tween(this.Q1option3).to(1, {position: cc.v2(+200,-250)}).start();
        }
        if(question == 2) {
        cc.tween(this.Q2option1).to(1, {position: cc.v2(-200,-250)}).start();
        cc.tween(this.Q2option2).to(1, {position: cc.v2(0,-250)}).start();
        cc.tween(this.Q2option3).to(1, {position: cc.v2(+200,-250)}).start();
        
        }
        cc.tween(this.option1box).to(1, {position: cc.v2(-200,-250)}).start();
        cc.tween(this.option2box).to(1, {position: cc.v2(0,-250)}).start();
        cc.tween(this.option3box).to(1, {position: cc.v2(+200,-250)}).start();  
    }

    //Question 1
    question1 () {
        cc.tween(this.rehna)
          .call(() => {
            this.DryBG.setPosition(cc.v2(0, 0));
            this.rehna.setPosition(cc.v2(300, -100));
            this.rehna.setScale(0.5);
            this.qtext.string = "";
            this.ftext_node.setPosition(cc.v2(0, -509));
            this.tryAgainButton.setPosition(cc.v2(0, -550));
            this.igloo.setPosition(cc.v2(-1300, -500));
            this.mudHouse.setPosition(cc.v2(-1300, 0));
            this.mudHouse_cut.setPosition(cc.v2(-1300, 0));
            this.woodHouse.setPosition(cc.v2(-1300, 500));
            this.smoke1.setPosition(cc.v2(-1500,0));
            this.smoke2.setPosition(cc.v2(-1500,0));
            this.smoke3.setPosition(cc.v2(-1500,0));
        }).delay(3)
        .call(() => {
            this.qtext.string = "Rehna is in a place which is really hot."; 
            this.intro_text(5);
            cc.audioEngine.play(this.Q1Dialogue1, false, 0.6);
        }).delay(6)
          .call(() => {
            this.qtext.string = "Drag the right kind of house for Rehna to stay\n comfortably."; 
            this.qtext_node.setPosition(cc.v2(-1500,350));
            cc.tween(this.qtext_node)
            .to(0.5, {
                position: cc.v2(0,350),
                opacity: 255
            }).start();
            cc.audioEngine.play(this.Q1Dialogue2, false, 0.6);
            this.optionTween(1);
        }).start();

        this.node.on('igloo', () => {
                this.igloo.setPosition(cc.v2(-50,-50));
                this.igloo_anim.playAdditive('igloo-melt');
                this.rehna_anim.playAdditive('q1-wrong').repeatCount = 3;
                cc.audioEngine.play(this.Q1igloo_melting, false, 0.6);
                cc.tween(this.igloo).by(1, {position: cc.v2(0,-20)}).start();
                cc.tween(this.tryAgainButton)
                .call(() => {
                    cc.audioEngine.play(this.wrongsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('intro-idle');
                    this.Nope.string = "Nope!";
                    this.ftext.string = "An igloo is made of snow. It will melt in a hot region.\nNot a good idea.";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q1igloo, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.tryAgainButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q1restart');
                }.bind(this));
        });

        this.node.on('wood', () => {
                this.woodHouse.setPosition(cc.v2(-50,0));
                this.rehna_anim.playAdditive('q1-wrong').repeatCount = 4;
                this.smoke1.setPosition(cc.v2(-108, 80));
                this.smoke2.setPosition(cc.v2(-70, 230));
                this.smoke3.setPosition(cc.v2(45, 75));
                this.smoke1_anim.playAdditive('smoke');
                this.smoke2_anim.playAdditive('smoke');
                this.smoke3_anim.playAdditive('smoke');
                cc.tween(this.tryAgainButton)
                .call(() => {
                    cc.audioEngine.play(this.wrongsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('intro-idle');
                    this.Nope.string = "Nope!";
                    this.ftext.string = "A wooden house would not keep Rehna cool in such hot weather.";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q1woodhouse, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.tryAgainButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q1restart');
                }.bind(this));
        });

        this.node.on('mud', () => {
                this.mudHouse.setPosition(cc.v2(-10,40));
                this.mudHouse_cut.setPosition(cc.v2(-80.131,-45.364));
                this.wind_anim.play('wind');
                cc.tween(this.wind).to(0, {position: cc.v2(375,-26)}).to(2, {position: cc.v2(-300,-26)}).start();
                cc.tween(this.rehna).to(4, {position: cc.v2(-70,-80)}).start();
                this.rehna_anim.play('q1-correct');
                cc.tween(this.nextButton)
                .call(() => {
                    cc.audioEngine.play(this.correctsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('q1-correct');
                    this.Nope.string = "";
                    this.Correct.string = "Correct!";
                    this.ftext.string = "Mud houses are constructed in desert regions because they help in\n keeping the inside cool. Large windows also help in air circulation";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q1mudhouse, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.nextButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q1complete')
                });
        });

        this.node.on('chosen', () => {
            console.log("Option chosen");
            cc.tween(this.Q1option1).to(0.5, {position: cc.v2(-200,-500)}).start();
            cc.tween(this.Q1option2).to(0.5, {position: cc.v2(0,-500)}).start();
            cc.tween(this.Q1option3).to(0.5, {position: cc.v2(+200,-500)}).start();
            cc.tween(this.option1box).to(0.5, {position: cc.v2(-200,-500)}).start();
            cc.tween(this.option2box).to(0.5, {position: cc.v2(0,-500)}).start();
            cc.tween(this.option3box).to(0.5, {position: cc.v2(+200,-500)}).start();
        });
    }

     //Question 2
    question2 () {
        cc.tween(this.rehna)
          .call(() => {
            this.SnowBG.setPosition(cc.v2(0, 0));
            this.rehna.setPosition(cc.v2(300, -100));
            this.rehna.setScale(0.5);
            this.rehna_anim.play('q2-idle');
            this.qtext.string = "";
            this.ftext_node.setPosition(cc.v2(0, -509));
            this.tryAgainButton.setPosition(cc.v2(0, -550));
            this.nextButton.setPosition(cc.v2(0, -550));
            this.igloo.setPosition(cc.v2(-1300, -500));
            this.igloo_cut.setPosition(cc.v2(-1300, 0));
            this.treeHouse.setPosition(cc.v2(-1300, 0));
            this.boatHouse.setPosition(cc.v2(-1300, 500));
        }).delay(3)
        .call(() => {
            this.qtext.string = "Brr.. Rehna is in a very cold region near the Arctic. She sure is\n adventurous."; 
            this.intro_text(8);
            cc.audioEngine.play(this.Q2Dialogue1, false, 0.6);
        }).delay(10)
          .call(() => {
            this.qtext.string = "Help her find a nice house to survive the extreme cold."; 
            this.qtext_node.setPosition(cc.v2(-1500,350));
            cc.tween(this.qtext_node)
            .to(0.5, {
                position: cc.v2(0,350),
                opacity: 255
            }).start();
            cc.audioEngine.play(this.Q2Dialogue2, false, 0.6);
            this.optionTween(2);
        }).start();

        this.node.on('igloo', () => {
                this.igloo.setPosition(cc.v2(-122.35,-92.318));
                this.igloo_cut.setPosition(cc.v2(-50, -50));
                cc.tween(this.rehna).to(4, {position: cc.v2(-70,-100)}).start();
                this.rehna_anim.play('q2-correct');
                cc.tween(this.nextButton)
                .call(() => {
                    cc.audioEngine.play(this.correctsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('q1-correct');
                    this.Nope.string = "";
                    this.Correct.string = "Correct!";
                    this.ftext.string = "An igloo is made of snow. It keeps the shelter warm because it has\n lots of air pockets. Also, its shape protects it from the wind.";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q2igloo, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.nextButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q2complete')
                });
        });

        this.node.on('tree', () => {
                this.treeHouse.setPosition(cc.v2(-50,20));
                this.rehna_anim.playAdditive('q2-wrong').repeatCount = 4;
                this.treeHouse_anim.playAdditive('tree_wrong').repeatCount = 4;
                cc.tween(this.tryAgainButton)
                .call(() => {
                    cc.audioEngine.play(this.wrongsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('q2-idle');
                    this.Nope.string = "Nope!";
                    this.ftext.string = "Sorry, there are no trees in the Arctic to build a treehouse.";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q2tree, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.tryAgainButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q2restart');
                }.bind(this));
        });

        this.node.on('boat', () => {
                this.boatHouse.setPosition(cc.v2(-50,0));
                this.rehna_anim.playAdditive('q2-wrong').repeatCount = 4;
                this.boatHouse_anim.playAdditive('boat_wrong').repeatCount = 4;
                cc.tween(this.tryAgainButton)
                .call(() => {
                    cc.audioEngine.play(this.wrongsfx, false, 0.6);
                }).delay(1)
                .call(() => {
                    this.rehna_anim.play('q2-idle');
                    this.Nope.string = "Nope!";
                    this.ftext.string = "Houseboats would do nothing to keep you warm. Plus, a lot of water\n in the Arctic is frozen.";
                    this.feedback_text();
                    cc.audioEngine.play(this.Q2boat, false, 0.6);
                }).delay(3)
                .to(1, {position: cc.v2(0,-300)})
                .start();
                this.tryAgainButton.on(cc.Node.EventType.TOUCH_START, function (Event) {
                    this.node.emit('q2restart');
                }.bind(this));
        });

        this.node.on('chosen', () => {
            cc.tween(this.Q2option1).to(0.5, {position: cc.v2(-200,-500)}).start();
            cc.tween(this.Q2option2).to(0.5, {position: cc.v2(0,-500)}).start();
            cc.tween(this.Q2option3).to(0.5, {position: cc.v2(+200,-500)}).start();
            cc.tween(this.option1box).to(0.5, {position: cc.v2(-200,-500)}).start();
            cc.tween(this.option2box).to(0.5, {position: cc.v2(0,-500)}).start();
            cc.tween(this.option3box).to(0.5, {position: cc.v2(+200,-500)}).start();
        });
    }
    
    // Start function
    start () {
        cc.audioEngine.play(this.BGM, true, 0.4);
        this.intro_walking();
        cc.tween(this.rehna).call(() => {
            this.qtext.string = "Meet Rehna, an adventurous 12 year old. Rehna is on a journey \n around the world.";
            this.intro_text(6);
            cc.audioEngine.play(this.introDialogue1, false, 0.6);
        }).delay(8)
          .call(() => {
            this.qtext.string = "She needs a place to stay while she travels. Help her find the\n appropriate shelter."; 
            this.intro_text(6);
            cc.audioEngine.play(this.introDialogue2, false, 0.6);
        }).delay(8)
          .call(() => {
            this.question1();
        })
        .start();

        this.node.on('q1restart', () => {
            this.question1();
        });
        this.node.on('q1complete', () => {
            this.question2();
        });
        this.node.on('q2restart', () => {
            this.question2();
        });
    }

    update (dt) {
        
    }
}
