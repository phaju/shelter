
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property({type: cc.Node})
    canvas: cc.Node = null;

    @property({type: cc.Node})
    dropZone: cc.Node = null;

    mouseDown = false;
    delta = null;
    boxFull = false;
    
    drag(thisNode: cc.Node){

        // Click
        thisNode.on(cc.Node.EventType.TOUCH_START, (Event)=> {
            this.mouseDown = true;
        });

        // Move
        thisNode.on(cc.Node.EventType.TOUCH_MOVE, (Event)=> {
            if(!this.mouseDown) 
                return;
            this.delta = Event.getDelta();
            thisNode.x += this.delta.x;
            thisNode.y += this.delta.y;
        });
        // Release
        thisNode.on(cc.Node.EventType.TOUCH_END, (Event)=> {
            this.mouseDown = false;
            if (this.dropZone.getBoundingBox().contains(this.dropZone.convertToNodeSpaceAR(this.node.convertToWorldSpaceAR(thisNode.getPosition()))) 
                && (this.boxFull == false)) {
                this.canvas.emit('chosen');
                thisNode.setPosition(this.dropZone.getPosition());
                this.boxFull = true;
                if(thisNode.name == "iGLOOICON2")
                    this.canvas.emit('igloo');
                else if(thisNode.name == "WOOD HOUSE ICON")
                    this.canvas.emit('wood');
                else if(thisNode.name == "BOATHOUSE-ICON")
                    this.canvas.emit('boat');
                else if(thisNode.name == "MUD-HOUSE-ICON2")
                    this.canvas.emit('mud');
                else if(thisNode.name == "TREEHOUSE-ICON")
                    this.canvas.emit('tree');
                }
            else {
                var pos;
                if(thisNode.name == "iGLOOICON2")
                    pos = cc.v2(-200, -250);
                else if(thisNode.name == "WOOD HOUSE ICON" || thisNode.name == "TREEHOUSE-ICON")
                    pos = cc.v2(0, -250);
                else if(thisNode.name == "MUD-HOUSE-ICON2" || thisNode.name == "Boat_house")
                    pos = cc.v2(200, -250);
                thisNode.setPosition(pos);
            }
        });
    }

    onLoad () {
        this.drag(this.node);
    }

    start () {

    }

    // update (dt) {}
}
